## Windows Server 2019 Build Script

If (-NOT ([Security.Principal.WindowsPrincipal]`
   [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
{
    Write-Warning "This script requires administrative privileges to run.  Please elevate and try again."
    Break
}

$ProgressPreference = 'SilentlyContinue'
Write-Output "Downloading Windows Server 2019 WIM..."
Invoke-WebRequest -Uri https://vtaimagebootstrap.blob.core.usgovcloudapi.net/wim-files/windows-server-2019.wim `
                  -OutFile artifacts/windows-server-2019.wim
$ProgressPreference = 'Continue'
Write-Output "Finished Downloading Windows Server 2019 WIM"

pushd windows-openstack-imaging-tools

Import-Module .\WinImageBuilder.psm1
Import-Module .\Config.psm1
Import-Module .\UnattendResources\ini.psm1

popd

New-WindowsOnlineImage -ConfigFilePath resources/configs/server2019.ini

Remove-Module WinImageBuilder
Remove-Module Config
Remove-Module ini

windows-openstack-imaging-tools\bin\qemu-img convert -f qcow2 -O raw artifacts\server2019.qcow2 artifacts\server2019.raw