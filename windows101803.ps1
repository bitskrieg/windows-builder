## Windows 10 1803 Build Script

If (-NOT ([Security.Principal.WindowsPrincipal]`
   [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
{
    Write-Warning "This script requires administrative privileges to run.  Please elevate and try again."
    Break
}

$ProgressPreference = 'SilentlyContinue'
Write-Output "Downloading Windows 10 1803 WIM..."
Invoke-WebRequest -Uri https://vtaimagebootstrap.blob.core.usgovcloudapi.net/wim-files/windows10-1803.wim `
                  -OutFile artifacts/windows10-1803.wim
$ProgressPreference = 'Continue'
Write-Output "Finished Downloading Windows 10 1803 WIM"

pushd windows-openstack-imaging-tools

Import-Module .\WinImageBuilder.psm1
Import-Module .\Config.psm1
Import-Module .\UnattendResources\ini.psm1

popd

New-WindowsOnlineImage -ConfigFilePath resources/configs/101803.ini

Remove-Module WinImageBuilder
Remove-Module Config
Remove-Module ini

windows-openstack-imaging-tools\bin\qemu-img convert -f qcow2 -O raw artifacts\win101803.qcow2  artifacts\win101803.raw