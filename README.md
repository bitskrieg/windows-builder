# Windows Builder Tools and Templates

This project assists with automatically building vta-optimized windows images.

To clone:

```
git clone --recurse-submodules << SSH or HTTP/S URL >>
```

See https://github.com/cloudbase/windows-openstack-imaging-tools for more 
information.