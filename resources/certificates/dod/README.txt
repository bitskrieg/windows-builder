####################################

Certificates_PKCS7_v5.3_DoD

####################################

File Information:

The structure of the file names for files that contain certificates in this bundle follow this pattern:

Certificates_PKCS7_[bundle version]_[group]_[subgroup information].[encoding].[file type]

where:
    'bundle version' is a PKE version identification number of the form Major.Minor.Release.
    'group' is a DoD PKI certificate grouping which is either DoD, ECA, JITC, SIPR, or WCF.
    'subgroup information' is additional details about which Root CA the file contains (with the Root's subordinates).
    'encoding' is the encoding scheme used in the file and is either der or pem.
    'file type' is the file extension which best fits the contents of the file.

There is a special which support the verification of this bundle. The file contains a CMS object which contains a
payload of file checksums included in this bundle and has been digitally signed by the DoD PKE code signing credential.
The struct of its file name follow this pattern:

Certificates_PKCS7_[bundle version]_[group].sha256

where:
    'bundle version' is a PKE version identification number of the form Major.Minor.Release.
    'group' is a DoD PKI certificate grouping which is either DoD, ECA, JITC, SIPR, or WCF.

Verification:


To verify this PKCS#7 package please perform the following steps:


1)  Verify Thumbprint on the first DoD_PKE_CA_chain.pem certificate using the following command:
openssl x509 -in DoD_PKE_CA_chain.pem -subject -issuer -fingerprint -noout


Verify the following output:
subject= /C=US/O=U.S. Government/OU=DoD/OU=PKI/CN=DoD Root CA 3
issuer= /C=US/O=U.S. Government/OU=DoD/OU=PKI/CN=DoD Root CA 3
SHA1 Fingerprint=D7:3C:A9:11:02:A2:20:4A:36:45:9E:D3:22:13:B4:67:D7:CE:97:FB

Confirm output and verify the DoD Root CA 3 SHA1 Fingerprint by calling the DoD PKI at (844) 347-2457 or DSN 850-0032.


2) Open DoD_PKE_CA_chain.pem in a text editor and confirm only two CERTIFICATE objects are present.


3) Verify the S/MIME signature on Certificates_PKCS7_v5.3_DoD.sha256 using the following command:
On Mac OSX:
$ openssl smime -verify -in Certificates_PKCS7_v5.3_DoD.sha256 -inform DER -CAfile DoD_PKE_CA_chain.pem | shasum -a 256 -c

On Linux:
$ openssl smime -verify -in Certificates_PKCS7_v5.3_DoD.sha256 -inform DER -CAfile DoD_PKE_CA_chain.pem | dos2unix | sha256sum -c

Verify the following output:
Verification successful

####################################

Usage:


Openssl - To export CA certificates to a concatenated PEM file for use as an openssl CAfile (named e.g. DoD_CAs.pem), use the following command:
openssl pkcs7 -in Certificates_PKCS7_v5.3_DoD.pem.p7b -print_certs -out DoD_CAs.pem

For more detailed instructions find our Getting Started with Firefox on Linux guide on our IASE PKI-PKE site. End Users > Web Browsers > Mozilla Firefox
