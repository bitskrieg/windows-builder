## Windows 10 1803 Build Script
## Set-ExecutionPolicy RemoteSigned
## Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All
## Create hyper-v virtual switch through hyper-v manager with the name external

If (-NOT ([Security.Principal.WindowsPrincipal]`
   [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
{
    Write-Warning "This script requires administrative privileges to run.  Please elevate and try again."
    Break
}

$ProgressPreference = 'SilentlyContinue'
Write-Output "Downloading Windows 10 1803 WIM..."
Invoke-WebRequest -Uri https://vtaimagebootstrap.blob.core.usgovcloudapi.net/wim-files/windows10-1803.wim `
                  -OutFile artifacts/windows10-1803.wim
$ProgressPreference = 'Continue'
Write-Output "Finished Downloading Windows 10 1803 WIM"

pushd windows-openstack-imaging-tools

## Temporary workaround for https://github.com/cloudbase/windows-openstack-imaging-tools/issues/219
(Get-Content WinImageBuilder.psm1).replace('($windowsImageConfig.cpu_count -gt [int](Get-TotalLogicalProcessors))', `
                                           '([int]$windowsImageConfig.cpu_count -gt [int](Get-TotalLogicalProcessors))') `
										   | Set-Content WinImageBuilder.psm1

Import-Module .\WinImageBuilder.psm1
Import-Module .\Config.psm1
Import-Module .\UnattendResources\ini.psm1

## installroot for dod certs or load certs from certificates.psm1 - http://iasecontent.disa.mil/pki-pke/InstallRoot_5.2x64.msi
## Start-Process msiexec.exe -Wait -ArgumentList '/I C:\installers\InstallRoot_5.2x64.msi /quiet'

popd

New-WindowsOnlineImage -ConfigFilePath resources/configs/101803-cac.ini

Remove-Module WinImageBuilder
Remove-Module Config
Remove-Module ini

windows-openstack-imaging-tools\bin\qemu-img convert artifacts\win101803.qcow2 -f raw artifacts\win101803-cac.raw
