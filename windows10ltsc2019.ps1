## Windows 10 2019 LTSC Build Script

If (-NOT ([Security.Principal.WindowsPrincipal]`
   [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator"))
{
    Write-Warning "This script requires administrative privileges to run.  Please elevate and try again."
    Break
}

$ProgressPreference = 'SilentlyContinue'
Write-Output "Downloading Windows 10 2019 LTSC WIM..."
Invoke-WebRequest -Uri https://vtaimagebootstrap.blob.core.usgovcloudapi.net/wim-files/windows10-2019-ltsc.wim `
                  -OutFile artifacts/windows10-2019-ltsc.wim
$ProgressPreference = 'Continue'
Write-Output "Finished Downloading Windows 10 2019 LTSC WIM"

pushd windows-openstack-imaging-tools

Import-Module .\WinImageBuilder.psm1
Import-Module .\Config.psm1
Import-Module .\UnattendResources\ini.psm1

popd

New-WindowsOnlineImage -ConfigFilePath resources/configs/102019ltsc.ini

Remove-Module WinImageBuilder
Remove-Module Config
Remove-Module ini

windows-openstack-imaging-tools\bin\qemu-img convert -f qcow2 -O raw artifacts\win10ltsc2019.qcow2 artifacts\win10ltsc2019.raw